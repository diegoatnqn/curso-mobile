import { Component, Inject, OnInit } from "@angular/core";
import * as imageSourceModule from "@nativescript/core/image-source";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ImageAsset } from "@nativescript/core";
import { DatabaseService } from "../domain/database.service";
import { AppState } from "../app.module";
import * as camera from "@nativescript/camera";
import { Image } from "@nativescript/core/ui/image"
import * as SocialShare from "nativescript-social-share";
import { Store } from "@ngrx/store";
import { Device, Screen } from "@nativescript/core/platform"; 
import { NoticiasEffects, NoticiasState } from "../domain/noticias-state.model";
import {
    connectionType,
    getConnectionType,
    startMonitoring,
    stopMonitoring
} from "@nativescript/core/connectivity"

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})

export class HomeComponent implements OnInit {
    
    leerHome: Array<string>;

    constructor(private miDb: DatabaseService, public lecturaHome: Store<AppState>) {
        lecturaHome.select((state) => state.noticias.lectura).subscribe((data) => {
            const le = data;
            if (le != null) {
                console.log("log suscripcion lectura");
                console.dir("data: ", le);
                this.leerHome = le;
            };
        });
    }

    ngOnInit(): void {
        this.doLater(this.onDatosPlataforma());
        this.doLater(this.onMonitoreoDatos());

    }
    doLater(fn) {
        setTimeout(fn, 2000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    borrarFavs() {
        this.miDb.getDb((db) => {
            //console.dir(db);
            db.db.execSQL("ALTER TABLE favoritos DROP COLUMN id,texto");
            }, (error) => {
                console.log("ERROR ELIMINAR FAVORITOS", error);
            });
    }
    botonCamara() {
        camera.requestPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                camera.takePicture(options).then( (imageAsset) => {
                        console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                        console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Foto guardada!");
                        imageSourceModule.fromAsset(imageAsset).then((imageSource) => {
                            SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
                        }).catch((err) => {
                                console.log("Error SHARE " + err.message);
                            });
                    }).catch((err) => {
                        console.log("Error TAKE PICTURE" + err.message);
                    });
            }, function failure() {
                console.log("Permiso de camara no aceptados");
            }
        );
    }

    onDatosPlataforma(): void {
        console.log("modelo", Device.model);
        console.log("tipo dispositivo", Device.deviceType);
        console.log("Sistema operativo", Device.os);
        console.log("versión sist operativo", Device.osVersion);
        console.log("Versión sdk", Device.sdkVersion);
        console.log("lenguaje", Device.language);
        console.log("fabricante", Device.manufacturer);
        console.log("código único de dispositivo", Device.uuid);
        console.log("altura en pixels normalizados", Screen.mainScreen.heightDIPs); // DIP
        //(Device Independent Pixel), también conocido como densidad de píxeles independientes.Un
        //píxel virtual que aparece aproximadamente del mismo tamaño en una variedad de
        //densidades de pantalla.
            console.log("altura pixels", Screen.mainScreen.heightPixels);
        console.log("escala pantalla", Screen.mainScreen.scale);
        console.log("ancho pixels normalizados", Screen.mainScreen.widthDIPs);
        console.log("ancho pixels", Screen.mainScreen.widthPixels);
    }

    monitoreando: boolean = false; // una variable para saber si estás monitoreando o no.

    onMonitoreoDatos(): void {
        const myConnectionType = getConnectionType();
        switch (myConnectionType) {
            case connectionType.none:
                console.log("Sin Conexion");
                break;
            case connectionType.wifi:
                console.log("WiFi");
                break;
            case connectionType.mobile:
                console.log("Mobile");
                break;
            case connectionType.ethernet:
                console.log("Ethernet"); // es decir, cableada
                break;
            case connectionType.bluetooth:
                console.log("Bluetooth");
                break;
            default:
                break;
        }
        this.monitoreando = !this.monitoreando;
        if (this.monitoreando) {
            startMonitoring((newConnectionType) => {
                switch (newConnectionType) {
                    case connectionType.none:
                        console.log("Cambió a sin conexión.");
                        break;
                    case connectionType.wifi:
                        console.log("Cambió a WiFi.");
                        break;
                    case connectionType.mobile:
                        console.log("Cambió a mobile.");
                        break;
                    case connectionType.ethernet:
                        console.log("Cambió a ethernet.");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth.");
                        break;
                    default:
                        break;
                }
            });
        } else {
            stopMonitoring();
        }
    } 
}
