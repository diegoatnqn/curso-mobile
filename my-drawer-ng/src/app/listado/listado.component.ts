import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ItemsService } from '../domain/items.service';

import * as SocialShare from "nativescript-social-share";
import { ImageSource } from "@nativescript/core/image-source";
import { Image } from '@nativescript/core';

@Component({
    selector: 'ns-listado',
    templateUrl: './listado.component.html',
    styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

    constructor(public items: ItemsService, private ruta: RouterExtensions) { }

    ngOnInit(): void {
        this.items.crear("Noticia 1");
        this.items.crear("Noticia 2");
        this.items.crear("Noticia 3");

    }
    onItemTap(x): void {     //Segun tap, irá siempre a DETALLE
        console.dir(x);
        switch (x.index) {
            case 0:
                this.ruta.navigate(['/detalle'], {
                    transition: {
                        name: "fade"
                    }
                }); break;
            case 1:
                this.ruta.navigate(['/detalle'], {
                    transition: {
                        name: "fade"
                    }
                }); break;
            case 2:
                this.ruta.navigate(['/detalle'], {
                    transition: {
                        name: "fade"
                    }
                }); break;
            default:
                this.ruta.navigate(['/detalle'], {
                    transition: {
                        name: "fade"
                    }
                }); break;
        }
    }
    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        let i = 3;
        setTimeout(() => {
            this.items.crear("Noticia " + i);
            pullRefresh.refreshing = false;
        }, 1000);
        i++;
    };

    onLongPress() {
        console.log("long press listado logo boca");
        let imgFromResources: ImageSource;
        ImageSource.fromUrl('https://icons.iconarchive.com/icons/iconsmind/outline/128/Smile-icon.png')
            .then((img) => {
                console.log("Job done!");
                console.dir(img);
                imgFromResources = img;
            });

        SocialShare.shareText("HOAL HOLA SHARE TEXT DESDE BOQUITA");
    }

}
