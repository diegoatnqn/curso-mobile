import { Injectable } from "@angular/core";
const couchbaseModule = require("nativescript-couchbase");
import { getJSON, request } from "@nativescript/core/http";
import { Couchbase } from "nativescript-couchbase";
import { DatabaseService } from "./database.service";

@Injectable()
export class NoticiasService {
    api: string = "https://dec56ea6d016.ngrok.io";
    database: Couchbase;
    
    constructor(public midb: DatabaseService,) {
        this.database = new couchbaseModule.Couchbase("test-database");
        midb.getDb((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error en getDb"));

        this.database.createView("logs", "1", (document, emitter) =>
            emitter.emit(document._id, document));
        const rows = this.database.executeQuery("logs", { limit: 200 });
        console.log("documentos: " + JSON.stringify(rows));
    }


    agregar(s: string) {
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({ nuevo: s })
        });
    }

    favs() {
        return getJSON(this.api + "/favs");
    }

    buscar(s: string) {
        this.midb.getDb((db) => {
            db.execSQL("INSERT INTO logs (texto) values (?)", [s], (err, id) => console.log("Nuevo id: ", id));
        }, () => console.log("eror en getDb"));

        const documentId = this.database.createDocument({ texto: s });
        console.log("nuevo id couchbase: ", documentId);

        return getJSON(this.api + "/get?q=" + s);
    }
}
