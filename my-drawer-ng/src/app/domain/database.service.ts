import { Injectable } from "@angular/core";
const sqlite = require("nativescript-sqlite");

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {

    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir db ", err);
            } else {
                console.log("Está la db abierta: ", db.isOpen() ? "Si" : "No");
                db.execSQL("CREATE TABLE IF NOT EXISTS logs(id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)").then((id) => {
                    console.log("CREATE TABLE OK");
                    fnOk(db);
                }, (error) => {
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                });
                db.execSQL("CREATE TABLE IF NOT EXISTS favoritos(id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT)").then((id) => {
                    console.log("CREATE FAVS OK");
                    fnOk(db);
                }, (error) => {
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                }
                );
            }
        })
    }
}
            

