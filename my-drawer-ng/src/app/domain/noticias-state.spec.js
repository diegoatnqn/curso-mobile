import { NoticiasState, intializeNoticiasState, InitMyDataAction, NuevaNoticiaAction, reducersNoticias } from "./noticias-state.model"
import { NoticiasService } from "./noticias.service";

describe(reducersNoticias, function () {
    it("should reduce init data", function () {
        // setup
        var prevState = NoticiasState.intializeNoticiasState();
        const action :InitMyDataAction = new InitMyDataAction(["noticia 1", "noticia 2"]);
        // action
        var newState : NoticiasState= new reducersNoticias(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].titulo).toEqual("noticia 1");
    });
});
