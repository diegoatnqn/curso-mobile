import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, GestureEventData, GridLayout } from "@nativescript/core";
import { DatabaseService } from "../domain/database.service";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
import { LeerNoticiaAction, Noticia } from "../domain/noticias-state.model";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {
    favs :Array<string>=[];

    constructor(private midb: DatabaseService, private store: Store<AppState>) {
        this.midb.getDb((db) => {
            //console.dir(db);
            db.each("select nombre from favoritos",
                (err, fila) => this.favs.push(fila),
                (err, totales) => console.log("Filas totales: ", totales));
        }, () => console.log("error en getDb"));
    }

    ngOnInit(): void {
        
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(args: GestureEventData) {
        console.log("Objeto que trigger evento: " + args.object);
        console.log("Vista que trigger evento: " + args.view);
        console.log("nombre de evento: " + args.eventName);

        const grid = <GridLayout>args.object;
        grid.animate({
            rotate: 360,
            duration: 1500
        });
    }

    mostrarFavs(): string[] {
        const misFavoritos = this.favs;
        console.log("Mostrar Favs to string");
        console.log(this.favs.toString());
        console.log("mostrar favs solo");
        console.log(this.favs);
        return misFavoritos
    }

    leerAhora(x: string) {
        console.dir(x);
        this.store.dispatch(new LeerNoticiaAction(x));
    }
    
}
