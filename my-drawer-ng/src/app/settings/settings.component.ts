import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toasts"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as SocialShare from "nativescript-social-share";
import * as dialogs from "@nativescript/core/ui/dialogs";
import { RouterExtensions } from '@nativescript/angular';
import { compose } from "nativescript-email";
let LS = require("nativescript-localstorage");


@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    
    constructor(private ruta: RouterExtensions) {
    }

    ngOnInit(): void {
        const toastOptions: Toast.ToastOptions = { text: "Hello world!", duration: Toast.DURATION.SHORT };
        this.doLater(() => Toast.show(toastOptions));
        this.obtenerNombre();
    }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onButtonTap(): void {
        console.log("Boton presionado");
        this.ruta.navigate(["/cambiar-nombre"], {
            transition: {
                name: "fade"
            }
        });
    }
    
    obtenerNombre(): string {
        const nombre = LS.getItem('nombreUsuario'); // 
        return nombre;
    }
    consolaNombre() {
        console.log(LS.getItem('nombreUsuario'));
        this.doLater(this.correo());
    }
    doLater(fn) {
        setTimeout(fn, 1000);
    }

    correo() {
        const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
        const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
        const appPath = appFolder.path; // esto te da el path a la carpeta src
        const logoPath = appPath + "/app/res/icon.png"; // aquí armas el path del archivo copiado
        compose({
            subject: "Mail de Prueba", // asunto del mail
            body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
            to: ["mail@mail.com"], //lista de destinatarios principales
            cc: [], //lista de destinatarios en copia
            bcc: [], //lista de destinatarios en copia oculta
            attachments: [ //listado de archivos adjuntos
                
            {
                fileName: "icon.png", // este archivo es el que lees directo del filesystem del mobile
                path: logoPath,
                mimeType: "image/png"
            }]
 }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err)); 
    }

    
    
}
