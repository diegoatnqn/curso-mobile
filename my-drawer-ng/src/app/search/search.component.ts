import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as TOAST from "nativescript-toasts";
import * as SocialShare from "nativescript-social-share";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color,} from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { android } from "@nativescript/core/application";
import { View } from "@nativescript/core/ui/core/view";
import { DatabaseService } from "../domain/database.service";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";
@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    public c: string;
    @ViewChild("layout") layout: ElementRef;
    @ViewChild("logo") logo: ElementRef;

    constructor(private noticias: NoticiasService,private store:Store<AppState>,public midb: DatabaseService) {
        
        this.c = "";
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida).subscribe((data) => {
            const f = data;
            if (f != null) {
                TOAST.show({ text: "Sugerimos leer: " + f.titulo, duration: TOAST.DURATION.SHORT });
            }
        });
        this.cambio();
        
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    };
    cambio() {       //METODO QUE SOLO SE EJECUTA EN ANDROID
        if (android) {
            this.c="ANDROID"
        }
    }
    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }
    buscarAhora(s: string) {
        console.dir("Buscar Ahora: " + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("Resultados Buscar Ahora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("Error buscar AHora: " + e);
            TOAST.show({ text: "Error en la busqueda", duration: TOAST.DURATION.SHORT });
        });

        const layout = <View>this.layout.nativeElement;
        const logo = <View>this.logo.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 300
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 300
        }));
        logo.animate({
            rotate: 360,
            duration: 3000
        });
    }
    agregarFavs(x:string) {
        this.midb.getDb((db) => {
            db.execSQL("INSERT INTO favoritos (nombre) values (?)", [x], (err, id) => console.log("Nuevo fav: ", x));
        }, () => console.log("eror en getDb"));
    }

    onLongPress(s: string) {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde app");
    }
}
