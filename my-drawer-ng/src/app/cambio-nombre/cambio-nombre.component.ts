import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { TextField } from '@nativescript/core';
@Component({
    selector: 'CambiarNombre',
    template:`
<!--
        <StackLayout orientation="vertical" width = "600" height="600"
             backgroundColor="lightgray" >
            <TextField [(ngModel)]="textFieldValue"
                hint="Ingresar texto..." required #nombre>
        </TextField>
        <Button text = "mostrar Value"(tap) = "mostrarValue(nombre)" > </Button>
    </StackLayout>
    <Button text = "Guardar!"(tap) = "onButtonTap(nombre)" > </Button>
-->
`
})
export class CambioNombreComponent implements OnInit {
    textFieldValue = "";

    constructor(private ruta: RouterExtensions) { }

    ngOnInit(): void {
        
  }
    onButtonTap(nuevo:any): void {
        localStorage.removeItem('nombreUsuario');
        console.log(this.textFieldValue);
        localStorage.setItem('nombreUsuario', nuevo.text);
        console.log("Volviendo a Settings ...");

        this.volver();
        
    }
    volver() {
        this.ruta.backToPreviousPage();
    }
    mostrarValue(s:any) {
        console.log(s.text);
    }
}
